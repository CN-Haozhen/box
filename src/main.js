import "bulma/css/bulma.min.css"
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/homepage/Page.vue'
import List from './components/merchantlist/Page.vue'
Vue.use(VueRouter);
import App from './App.vue'
import "swiper/dist/css/swiper.min.css";

Vue.config.productionTip = false

export default new VueRouter({
  routes: [
    {
      path:'/List',
      name:'List',
      components: List
    },
    {
      path:'/Home',
      name:'Home',
      component: Home
    }
  ]
})

new Vue({
  render: h => h(App),
}).$mount('#app')
